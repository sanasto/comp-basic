# Sanasto comp-basic

Tämä sanasto sisältää peruskäsitteitä (*termejä*) **tietokoneista** suomeksi ja englanniksi.

Sanaston lähteinä on käytetty yleistä alan tietämystä sekä suomen ja englanninkielistä wikipediaa.

Sanasto on koneluettavassa JSON -muodossa.

## Rakenne

Ylimmän tason rakenne on aina taulukko. Taulukon sisällä olevat JSON -objektit sisältävät yhden käsitteen eli termin määrittelyn.

```JSON
[
   {
      "id": "algorithm",
      "english": "Algorithm",
      "englishLink": "https://en.wikipedia.org/wiki/Algorithm",
      "finnishLink": "https://fi.wikipedia.org/wiki/Algoritmi",
      "finnish": "Algoritmi",
      "definition": "Yksityiskohtainen kuvaus tai ohje siitä, miten tehtävä tai prosessi suoritetaan; jota seuraamalla voidaan ratkaista tietty ongelma."
   },

```

Termin elementit ovat:

* `id`: elementin tunniste, ei tyypillisesti näy käyttäjälle. Oltava yksikäsitteinen JSON -tiedostossa.
* `english`: termi englanniksi.
* `englishLink`: HTTP -linkki termiä kuvaavaan englanninkieliseen sivustoon jota on käytetty termin kuvauksen lähteenä.
* `finnishLink`: HTTP -linkki termiä kuvaavaan suomenkieliseen sivustoon jota on käytetty termin kuvauksen lähteenä.
* `finnish`: termi suomeksi. Jos sopivaa käännöstä ei löydy, sama kuin englanninkielinen termi.
* `definition`: termin määrittely suomeksi. Määrittely voi olla lause tai 2-3 lyhyttä kappaletta. Sanastoa voidaan lukea älypuhelinten näytöltä, joten määritelmän pitäisi olla lyhyt ja ytimekäs. 

Jokaisen termin (JSON -objektin) `id` -elementin arvon on oltava uniikki sanaston sisällä. Arvo ei tyypillisesti näy käyttäjälle, joten se se voi olla myös numeerinen (mutta silti merkkijono) tai vaikkapa [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier).

Jokaisen elementin (`id`, `english`, `englishLink`, `finnishLink`, `finnish` ja `definition`) avain *on esiinnyttävä* JSON -objektissa, mutta sen arvo voi olla tyhjä merkkijono.

`definition` -elementin merkkijono *voi sisältää* yksinkertaisia [Markdown](https://www.markdownguide.org) -muotoiluja (*kursiivi*, **vahvennus**, `koodi`). Mahdolliset tekstissä olevat lainausmerkit on esitettävä tekstissä escapattuna:

```console
Jos tekstissä on \"lainausmerkki\" se on esitettävä edellä mainitulla tavalla.
```

Elementtien järjestyksellä JSON -objektin sisällä ei ole merkitystä.

## Kontribuutiot

Jos huomaat sanastossa virheitä tai puutteita, voit tehdä tästä Issuen, tai lähettää minulle sähköpostia. Täydennyksiä voi tehdä myös forkkaamalla projektin ja tekemällä pull requestin. Huolehdi että tekemiesi täydennysten sisältö sopii *tähän* sanastoon, kieli on siistiä, linkit lähteisiin (jotka sallivat sisällön hyödyntämisen muualla) toimivat ja JSON on validia.

Jos muokkaat JSON -tiedostoa, **tarkista JSON:n validisuus** esimerkiksi [JSONLint -palvelusssa](https://jsonlint.com)

## Lisenssi

Creative Commons Attribution-ShareAlike 3.0 Unported License

Sanaston on laatinut Antti Juustila.
(c) Antti Juustila, 2022.